var LifeInTheUK;
(function (LifeInTheUK) {
    angular.module('LifeInTheUKApp')
        .service('dataService', LifeInTheUK.Services.DataService)
        .controller('IndexController', LifeInTheUK.Controllers.IndexController)
        .controller('ChapterController', LifeInTheUK.Controllers.ChapterController)
        .controller('MainController', LifeInTheUK.Controllers.MainController)
        .directive('section', LifeInTheUK.Directives.SectionDirective.factory())
        .directive('subsection', LifeInTheUK.Directives.SubsectionDirective.factory())
        .directive('chapter', LifeInTheUK.Directives.ChapterDirective.factory())
        .directive('fact', LifeInTheUK.Directives.FactDirective.factory())
        .directive('tags', LifeInTheUK.Directives.TagsDirective.factory())
        .directive('tag', LifeInTheUK.Directives.TagDirective.factory())
        .directive('menu', LifeInTheUK.Directives.MenuDirective.factory())
        .config(['$routeProvider', '$locationProvider', ($routeProvider, $locationProvider) => {
            $routeProvider.when('/', {
                controller: 'IndexController',
                templateUrl: '/ts/app/templates/pages/index.html'
            }).when('/main', {
                controller: 'MainController',
                templateUrl: '/ts/app/templates/pages/main.html'
            }).when('/chapters/:id', {
                controller: 'ChapterController',
                templateUrl: '/ts/app/templates/pages/chapter.html'
            }).when('/index', {
                redirectTo: '/'
            }).otherwise({
                redirectTo: '/'
            });
        }]);
})(LifeInTheUK || (LifeInTheUK = {}));
//# sourceMappingURL=app.js.map