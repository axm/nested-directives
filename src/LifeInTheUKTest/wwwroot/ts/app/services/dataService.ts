﻿namespace LifeInTheUK.Services {

	export interface IDataService {
	}

	export class DataService implements IDataService {
		private _data: Models.DataBlob = new Models.DataBlob();
		public hasData: boolean = false;

		get data(): Models.DataBlob {
			return this._data;
		}

		set data(newData: Models.DataBlob) {
			this._data = newData;

			this.hasData = true;
		}

		constructor() {
			
		}

		public reset(): void {
			this.data = new Models.DataBlob();
		}

		public getChapter(id: number): Models.Chapter {
			for (const chapter of this._data.chapters) {
				if (chapter.id == id) {
					return chapter;
				}
			}

			return null;
		}
	}
}