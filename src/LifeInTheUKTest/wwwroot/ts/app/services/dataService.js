var LifeInTheUK;
(function (LifeInTheUK) {
    var Services;
    (function (Services) {
        class DataService {
            constructor() {
                this._data = new LifeInTheUK.Models.DataBlob();
                this.hasData = false;
            }
            get data() {
                return this._data;
            }
            set data(newData) {
                this._data = newData;
                this.hasData = true;
            }
            reset() {
                this.data = new LifeInTheUK.Models.DataBlob();
            }
            getChapter(id) {
                for (const chapter of this._data.chapters) {
                    if (chapter.id == id) {
                        return chapter;
                    }
                }
                return null;
            }
        }
        Services.DataService = DataService;
    })(Services = LifeInTheUK.Services || (LifeInTheUK.Services = {}));
})(LifeInTheUK || (LifeInTheUK = {}));
//# sourceMappingURL=dataService.js.map