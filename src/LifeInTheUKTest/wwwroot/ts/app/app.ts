﻿namespace LifeInTheUK {
	angular.module('LifeInTheUKApp')
		.service('dataService', Services.DataService)
		.controller('IndexController', Controllers.IndexController)
		.controller('ChapterController', Controllers.ChapterController)
		.controller('MainController', Controllers.MainController)
		.directive('section', Directives.SectionDirective.factory())
		.directive('subsection', Directives.SubsectionDirective.factory())
		.directive('chapter', Directives.ChapterDirective.factory())
		.directive('fact', Directives.FactDirective.factory())
		.directive('tags', Directives.TagsDirective.factory())
		.directive('tag', Directives.TagDirective.factory())
		.directive('menu', Directives.MenuDirective.factory())
		.config(['$routeProvider', '$locationProvider', ($routeProvider, $locationProvider) => {
			$routeProvider.when('/', {
				controller: 'IndexController',
				templateUrl: '/ts/app/templates/pages/index.html'
			}).when('/main', {
				controller: 'MainController',
				templateUrl: '/ts/app/templates/pages/main.html'
			}).when('/chapters/:id', {
				controller: 'ChapterController',
				templateUrl: '/ts/app/templates/pages/chapter.html'
			}).when('/index', {
				redirectTo: '/'
			}).otherwise({
				redirectTo: '/'
			});
		}])
}