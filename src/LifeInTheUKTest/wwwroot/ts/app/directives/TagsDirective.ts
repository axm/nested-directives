﻿namespace LifeInTheUK.Directives {
	export class TagsDirective implements ng.IDirective {
		templateUrl: string = "/ts/app/templates/directives/tags.html";

		constructor() {

		}

		link(scope: ng.IScope, instanceElement: JQuery, instanceAttributes: ng.IAttributes): void {

		}

		static factory(): ng.IDirectiveFactory {
			const directive: ng.IDirectiveFactory = () => new TagsDirective();
			directive.$inject = [];

			return directive;
		}
	}
}