﻿namespace LifeInTheUK.Directives {
	export class SectionDirective implements ng.IDirective {
		templateUrl: string = "/ts/app/templates/directives/section.html";

		constructor() {

		}

		link(scope: ng.IScope, instanceElement: JQuery, instanceAttributes: ng.IAttributes): void {

		}
		static factory(): ng.IDirectiveFactory {
			const directive: ng.IDirectiveFactory = () => new SectionDirective();
			directive.$inject = [];

			return directive;
		}
	}
}