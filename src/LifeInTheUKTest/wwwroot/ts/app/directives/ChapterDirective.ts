﻿namespace LifeInTheUK.Directives {
	export class ChapterDirective implements ng.IDirective {
		templateUrl: string = "/ts/app/templates/directives/chapter.html";
		restrict: string = "E";
		title: string = "Chapter 1";

		constructor() {

		}

		link(scope: ng.IScope, instanceElement: JQuery, instanceAttributes: ng.IAttributes): void {

		}

		static factory(): ng.IDirectiveFactory {
			const directive: ng.IDirectiveFactory = () => new ChapterDirective();
			directive.$inject = [];

			return directive;
		}
	}
}