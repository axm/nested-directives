﻿namespace LifeInTheUK.Directives {
	export class MenuDirective implements ng.IDirective {
		private chapters: Array<Models.Chapter> = [];

		constructor(private dataService: Services.DataService) {
			//this.chapters = dataService.data.chapters;
		}

		static factory(): ng.IDirectiveFactory {
			const directive = (dataService: Services.DataService) => new MenuDirective(dataService);
			directive.$inject = ['dataService'];

			return directive;
		}
	}
}