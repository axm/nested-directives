var LifeInTheUK;
(function (LifeInTheUK) {
    var Directives;
    (function (Directives) {
        class TagsDirective {
            constructor() {
                this.templateUrl = "/ts/app/templates/directives/tags.html";
            }
            link(scope, instanceElement, instanceAttributes) {
            }
            static factory() {
                const directive = () => new TagsDirective();
                directive.$inject = [];
                return directive;
            }
        }
        Directives.TagsDirective = TagsDirective;
    })(Directives = LifeInTheUK.Directives || (LifeInTheUK.Directives = {}));
})(LifeInTheUK || (LifeInTheUK = {}));
//# sourceMappingURL=TagsDirective.js.map