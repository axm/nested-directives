var LifeInTheUK;
(function (LifeInTheUK) {
    var Directives;
    (function (Directives) {
        class ChapterDirective {
            constructor() {
                this.templateUrl = "/ts/app/templates/directives/chapter.html";
                this.restrict = "E";
                this.title = "Chapter 1";
            }
            link(scope, instanceElement, instanceAttributes) {
            }
            static factory() {
                const directive = () => new ChapterDirective();
                directive.$inject = [];
                return directive;
            }
        }
        Directives.ChapterDirective = ChapterDirective;
    })(Directives = LifeInTheUK.Directives || (LifeInTheUK.Directives = {}));
})(LifeInTheUK || (LifeInTheUK = {}));
//# sourceMappingURL=ChapterDirective.js.map