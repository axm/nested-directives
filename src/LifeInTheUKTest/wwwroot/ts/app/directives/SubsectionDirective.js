var LifeInTheUK;
(function (LifeInTheUK) {
    var Directives;
    (function (Directives) {
        class SubsectionDirective {
            constructor() {
                this.templateUrl = "/ts/app/templates/directives/subsection.html";
            }
            link(scope, instanceElement, instanceAttributes) {
            }
            static factory() {
                const directive = () => new SubsectionDirective();
                directive.$inject = [];
                return directive;
            }
        }
        Directives.SubsectionDirective = SubsectionDirective;
    })(Directives = LifeInTheUK.Directives || (LifeInTheUK.Directives = {}));
})(LifeInTheUK || (LifeInTheUK = {}));
//# sourceMappingURL=SubsectionDirective.js.map