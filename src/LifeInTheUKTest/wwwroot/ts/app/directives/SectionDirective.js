var LifeInTheUK;
(function (LifeInTheUK) {
    var Directives;
    (function (Directives) {
        class SectionDirective {
            constructor() {
                this.templateUrl = "/ts/app/templates/directives/section.html";
            }
            link(scope, instanceElement, instanceAttributes) {
            }
            static factory() {
                const directive = () => new SectionDirective();
                directive.$inject = [];
                return directive;
            }
        }
        Directives.SectionDirective = SectionDirective;
    })(Directives = LifeInTheUK.Directives || (LifeInTheUK.Directives = {}));
})(LifeInTheUK || (LifeInTheUK = {}));
//# sourceMappingURL=SectionDirective.js.map