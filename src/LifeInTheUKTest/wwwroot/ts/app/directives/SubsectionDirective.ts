﻿namespace LifeInTheUK.Directives {
	export class SubsectionDirective implements ng.IDirective {
		templateUrl: string = "/ts/app/templates/directives/subsection.html";

		constructor() {
			
		}

		link(scope: ng.IScope, instanceElement: JQuery, instanceAttributes: ng.IAttributes): void {

		}

		static factory(): ng.IDirectiveFactory {
			const directive: ng.IDirectiveFactory = () => new SubsectionDirective();
			directive.$inject = [];

			return directive;
		}
	}
}