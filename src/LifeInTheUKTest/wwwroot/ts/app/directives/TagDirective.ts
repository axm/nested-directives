﻿namespace LifeInTheUK.Directives {
	export class TagDirective implements ng.IDirective {
		templateUrl: string = "/ts/app/templates/directives/tag.html";

		constructor() {

		}

		link(scope: ng.IScope, instanceElement: JQuery, instanceAttributes: ng.IAttributes): void {

		}

		static factory(): ng.IDirectiveFactory {
			const directive: ng.IDirectiveFactory = () => new TagDirective();
			directive.$inject = [];

			return directive;
		}
	}
}