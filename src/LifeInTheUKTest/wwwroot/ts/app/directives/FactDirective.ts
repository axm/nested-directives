﻿namespace LifeInTheUK.Directives {
	export class FactDirective implements ng.IDirective {
		templateUrl: string = "/ts/app/templates/directives/fact.html";

		constructor() {

		}

		link(scope: ng.IScope, instanceElement: JQuery, instanceAttributes: ng.IAttributes): void {

		}
		static factory(): ng.IDirectiveFactory {
			const directive: ng.IDirectiveFactory = () => new FactDirective();
			directive.$inject = [];

			return directive;
		}
	}
}