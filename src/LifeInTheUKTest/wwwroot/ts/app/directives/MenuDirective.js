var LifeInTheUK;
(function (LifeInTheUK) {
    var Directives;
    (function (Directives) {
        class MenuDirective {
            constructor(dataService) {
                this.dataService = dataService;
                this.chapters = [];
                //this.chapters = dataService.data.chapters;
            }
            static factory() {
                const directive = (dataService) => new MenuDirective(dataService);
                directive.$inject = ['dataService'];
                return directive;
            }
        }
        Directives.MenuDirective = MenuDirective;
    })(Directives = LifeInTheUK.Directives || (LifeInTheUK.Directives = {}));
})(LifeInTheUK || (LifeInTheUK = {}));
//# sourceMappingURL=MenuDirective.js.map