var LifeInTheUK;
(function (LifeInTheUK) {
    var Directives;
    (function (Directives) {
        class FactDirective {
            constructor() {
                this.templateUrl = "/ts/app/templates/directives/fact.html";
            }
            link(scope, instanceElement, instanceAttributes) {
            }
            static factory() {
                const directive = () => new FactDirective();
                directive.$inject = [];
                return directive;
            }
        }
        Directives.FactDirective = FactDirective;
    })(Directives = LifeInTheUK.Directives || (LifeInTheUK.Directives = {}));
})(LifeInTheUK || (LifeInTheUK = {}));
//# sourceMappingURL=FactDirective.js.map