var LifeInTheUK;
(function (LifeInTheUK) {
    var Directives;
    (function (Directives) {
        class TagDirective {
            constructor() {
                this.templateUrl = "/ts/app/templates/directives/tag.html";
            }
            link(scope, instanceElement, instanceAttributes) {
            }
            static factory() {
                const directive = () => new TagDirective();
                directive.$inject = [];
                return directive;
            }
        }
        Directives.TagDirective = TagDirective;
    })(Directives = LifeInTheUK.Directives || (LifeInTheUK.Directives = {}));
})(LifeInTheUK || (LifeInTheUK = {}));
//# sourceMappingURL=TagDirective.js.map