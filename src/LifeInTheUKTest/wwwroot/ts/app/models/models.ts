﻿namespace LifeInTheUK.Models {
	export class DataBlob {
		public chapters: Array<Chapter> = [];
	}

	export class Chapter {
		public id: number;
		public title: string;
		public sections: Array<Section>;
		public tags: Array<Tag>;
	}

	export class Section {
		public id: number;
		public title: string;
		public tags: Array<Tag>;
		public subsections: Array<Subsection>;
	}

	export class Subsection {
		public id: number;
		public title: string;
		public tags: Array<Tag>;
		public facts: Array<Fact>;
	}

	export class Fact {
		public year: number;
		public description: string;
		public tags: Array<Tag>;
	}

	export class Tag {
		public title: string;
	}
}