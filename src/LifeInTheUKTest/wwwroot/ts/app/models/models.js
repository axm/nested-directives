var LifeInTheUK;
(function (LifeInTheUK) {
    var Models;
    (function (Models) {
        class DataBlob {
            constructor() {
                this.chapters = [];
            }
        }
        Models.DataBlob = DataBlob;
        class Chapter {
        }
        Models.Chapter = Chapter;
        class Section {
        }
        Models.Section = Section;
        class Subsection {
        }
        Models.Subsection = Subsection;
        class Fact {
        }
        Models.Fact = Fact;
        class Tag {
        }
        Models.Tag = Tag;
    })(Models = LifeInTheUK.Models || (LifeInTheUK.Models = {}));
})(LifeInTheUK || (LifeInTheUK = {}));
//# sourceMappingURL=models.js.map