﻿/// <reference path="../../../lib/typings/angular-route.d.ts" />

namespace LifeInTheUK.Controllers {
	export class ChapterController implements ng.IController {
		static $inject: Array<string> = ["$scope", "dataService", "$routeParams"];

		constructor(private $scope: ng.IScope, private dataService: Services.DataService, private $routeParams) {
			const id = $routeParams.id;

			$scope['chapter'] = dataService.getChapter(id);
		}
	}
}