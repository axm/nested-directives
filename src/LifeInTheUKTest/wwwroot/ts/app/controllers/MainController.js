/// <reference path="../models/models.ts" />
var LifeInTheUK;
(function (LifeInTheUK) {
    var Controllers;
    (function (Controllers) {
        class MainController {
            //private chapters: Array<Models.Chapter> = [];
            constructor($scope, $http, $location, dataService) {
                //$http.get('http://localhost:62678/api/data').then((response) => {
                //	this.buildData(response, this.chapters);
                //}, this.handleError);
                this.$scope = $scope;
                this.$http = $http;
                this.$location = $location;
                this.dataService = dataService;
                if (!this.dataService.hasData) {
                    $location.path('/');
                }
                $scope['chapters'] = this.dataService.data.chapters;
                //this.chapters = new Array<Models.Chapter>();
                //this.chapters.push({
                //	title: "Life in the UK 1",
                //	sections: [
                //		{
                //			title: 'Section 1',
                //			subsections: [],
                //			tags: []
                //		},
                //		{
                //			title: 'Section 2',
                //			subsections: [],
                //			tags: []
                //		}
                //	],
                //	tags: []
                //});
            }
        }
        MainController.$inject = ['$scope', '$http', "$location", "dataService"];
        Controllers.MainController = MainController;
        class ModelFactory {
            static buildChapter() {
                const chapter = new LifeInTheUK.Models.Chapter();
                return chapter;
            }
            static buildSection() {
                const section = new LifeInTheUK.Models.Section();
                return section;
            }
        }
        Controllers.ModelFactory = ModelFactory;
    })(Controllers = LifeInTheUK.Controllers || (LifeInTheUK.Controllers = {}));
})(LifeInTheUK || (LifeInTheUK = {}));
//# sourceMappingURL=MainController.js.map