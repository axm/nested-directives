var LifeInTheUK;
(function (LifeInTheUK) {
    var Controllers;
    (function (Controllers) {
        class IndexController {
            constructor($scope, $http, $location, dataService) {
                this.$scope = $scope;
                this.$http = $http;
                this.$location = $location;
                this.dataService = dataService;
                $http.get('http://localhost:62678/api/data').then((response) => {
                    this.dataService.reset();
                    this.buildData(response, this.dataService);
                    $location.path('/main');
                });
            }
            buildData(response, dataService) {
                //const chapters: Array<Models.Chapter> = [];
                for (const c of response.data.chapters) {
                    const chapter = new LifeInTheUK.Models.Chapter();
                    chapter.id = c.id;
                    chapter.title = c.title;
                    chapter.sections = [];
                    chapter.tags = [];
                    for (const s of c.sections) {
                        const section = new LifeInTheUK.Models.Section();
                        section.title = s.title;
                        section.subsections = [];
                        for (const ss of s.subsections) {
                            const subsection = new LifeInTheUK.Models.Subsection();
                            subsection.tags = [];
                            subsection.title = ss.title;
                            subsection.facts = [];
                            for (const f of ss.facts) {
                                const fact = new LifeInTheUK.Models.Fact();
                                fact.year = f.year;
                                fact.tags = [];
                                fact.description = f.description;
                                for (const _t of f.tags) {
                                    const _tag = new LifeInTheUK.Models.Tag();
                                    _tag.title = _t;
                                    fact.tags.push(_tag);
                                }
                                subsection.facts.push(fact);
                            }
                            section.subsections.push(subsection);
                        }
                        chapter.sections.push(section);
                    }
                    for (const t of c.tags) {
                        chapter.tags.push({
                            title: t
                        });
                    }
                    dataService.data.chapters.push(chapter);
                }
                this.dataService.hasData = true;
            }
            handleError(response) {
            }
        }
        IndexController.$inject = ["$scope", "$http", "$location", "dataService"];
        Controllers.IndexController = IndexController;
    })(Controllers = LifeInTheUK.Controllers || (LifeInTheUK.Controllers = {}));
})(LifeInTheUK || (LifeInTheUK = {}));
//# sourceMappingURL=IndexController.js.map