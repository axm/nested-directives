﻿/// <reference path="../models/models.ts" />

namespace LifeInTheUK.Controllers {
	export class MainController implements ng.IController {
		static $inject = ['$scope', '$http', "$location", "dataService"];
		//private chapters: Array<Models.Chapter> = [];

		constructor(private $scope: ng.IScope, private $http: ng.IHttpService, private $location: ng.ILocationService, private dataService: Services.DataService) {
			//$http.get('http://localhost:62678/api/data').then((response) => {
			//	this.buildData(response, this.chapters);
			//}, this.handleError);

			if (!this.dataService.hasData) {
				$location.path('/');
			}

			$scope['chapters'] = this.dataService.data.chapters;

			//this.chapters = new Array<Models.Chapter>();
			//this.chapters.push({
			//	title: "Life in the UK 1",
			//	sections: [
			//		{
			//			title: 'Section 1',
			//			subsections: [],
			//			tags: []
			//		},
			//		{
			//			title: 'Section 2',
			//			subsections: [],
			//			tags: []
			//		}
			//	],
			//	tags: []
			//});
		}

		//private buildData(response: any, chapters: Array<Models.Chapter>): void {
		//	//const chapters: Array<Models.Chapter> = [];

		//	for (const c of response.data.chapters) {
		//		const chapter = new Models.Chapter();
		//		chapter.title = c.title;
		//		chapter.sections = [];
		//		chapter.tags = [];

		//		for (const s of c.sections) {
		//			const section = new Models.Section();
		//			section.title = s.title;
		//			section.subsections = [];

		//			for (const ss of s.subsections) {
		//				const subsection = new Models.Subsection();
		//				subsection.tags = [];
		//				subsection.title = ss.title;
		//				subsection.facts = [];

		//				for (const f of ss.facts) {
		//					const fact = new Models.Fact();
		//					fact.year = f.year;
		//					fact.tags = [];
		//					fact.description = f.description;

		//					for (const _t of f.tags) {
		//						const _tag = new Models.Tag();
		//						_tag.title = _t;

		//						fact.tags.push(_tag);
		//					}

		//					subsection.facts.push(fact);
		//				}

		//				section.subsections.push(subsection);

		//			}

		//			chapter.sections.push(section);
		//		}

		//		for (const t of c.tags) {
		//			chapter.tags.push({
		//				title: t
		//			});
		//		}

		//		chapters.push(chapter);
		//	}
		//}

		//private handleError(response: any): void {

		//}
	}

	export class ModelFactory {
		static buildChapter(): Models.Chapter {
			const chapter = new Models.Chapter();

			return chapter;
		}

		static buildSection(): Models.Section {
			const section = new Models.Section();

			return section;
		}
	}
}