/// <reference path="../../../lib/typings/angular-route.d.ts" />
var LifeInTheUK;
(function (LifeInTheUK) {
    var Controllers;
    (function (Controllers) {
        class ChapterController {
            constructor($scope, dataService, $routeParams) {
                this.$scope = $scope;
                this.dataService = dataService;
                this.$routeParams = $routeParams;
                const id = $routeParams.id;
                $scope['chapter'] = dataService.getChapter(id);
            }
        }
        ChapterController.$inject = ["$scope", "dataService", "$routeParams"];
        Controllers.ChapterController = ChapterController;
    })(Controllers = LifeInTheUK.Controllers || (LifeInTheUK.Controllers = {}));
})(LifeInTheUK || (LifeInTheUK = {}));
//# sourceMappingURL=ChapterController.js.map