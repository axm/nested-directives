﻿namespace LifeInTheUK.Controllers {
	export class IndexController implements ng.IController {
		static $inject: Array<string> = ["$scope", "$http", "$location", "dataService"];

		constructor(private $scope: ng.IScope, private $http: ng.IHttpService, private $location: ng.ILocationService, private dataService: Services.DataService) {
			$http.get('http://localhost:62678/api/data').then((response) => {
				this.dataService.reset();

				this.buildData(response, this.dataService);

				$location.path('/main');
			});
		}

		private buildData(response: any, dataService: Services.DataService): void {
			//const chapters: Array<Models.Chapter> = [];

			for (const c of response.data.chapters) {
				const chapter = new Models.Chapter();
				chapter.id = c.id;
				chapter.title = c.title;
				chapter.sections = [];
				chapter.tags = [];

				for (const s of c.sections) {
					const section = new Models.Section();
					section.title = s.title;
					section.subsections = [];

					for (const ss of s.subsections) {
						const subsection = new Models.Subsection();
						subsection.tags = [];
						subsection.title = ss.title;
						subsection.facts = [];

						for (const f of ss.facts) {
							const fact = new Models.Fact();
							fact.year = f.year;
							fact.tags = [];
							fact.description = f.description;

							for (const _t of f.tags) {
								const _tag = new Models.Tag();
								_tag.title = _t;

								fact.tags.push(_tag);
							}

							subsection.facts.push(fact);
						}

						section.subsections.push(subsection);

					}

					chapter.sections.push(section);
				}

				for (const t of c.tags) {
					chapter.tags.push({
						title: t
					});
				}

				dataService.data.chapters.push(chapter);
			}

			this.dataService.hasData = true;
		}

		private handleError(response: any): void {

		}
	}
}