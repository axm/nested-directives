﻿"use strict";

var gulp = require("gulp"),
	rimraf = require("rimraf"),
	concat = require("gulp-concat"),
	cssmin = require("gulp-cssmin"),
	uglify = require("gulp-uglify"),
	jasmine = require("gulp-jasmine"),
	jasmineBrowser = require("gulp-jasmine-browser"),
	rename = require("gulp-rename"),
	logger = require("gulp-logger");

var paths = {
	webroot: "./wwwroot/"
};

paths.models = paths.webroot + "ts/models/*.js";
paths.js = paths.webroot + "js/**/*.js";
paths.jsDest = paths.webroot + "js";
paths.compiledJs = paths.webroot + "ts/**/*.js";
paths.compiledJsList = [
	paths.webroot + "ts/app/init.js",
	paths.webroot + "ts/app/models/*.js",
	paths.webroot + "ts/app/services/*.js",
	paths.webroot + "ts/app/controllers/*.js",
	paths.webroot + "ts/app/directives/*.js",
	paths.webroot + "ts/app/app.js",
];
paths.tsList = [
	paths.webroot + "ts/app/init.ts",
	paths.webroot + "ts/app/models/*.ts",
	paths.webroot + "ts/app/services/*.ts",
	paths.webroot + "ts/app/controllers/*.ts",
	paths.webroot + "ts/app/directives/*.ts",
	paths.webroot + "ts/app/app.ts",
];
paths.minJs = paths.webroot + "js/**/*.min.js";
paths.css = paths.webroot + "css/**/*.css";
paths.minCss = paths.webroot + "css/**/*.min.css";
paths.concatJsDest = paths.webroot + "js/site.js";
paths.concatTsDest = paths.webroot + "js/site.ts";
paths.compiledJsDest = paths.webroot + "js/site.min.js";
paths.concatCssDest = paths.webroot + "css/site.min.css";

gulp.task("clean:js", function (cb) {
	rimraf(paths.concatJsDest, cb);
});

gulp.task("clean", ["clean:js"]);

gulp.task("concat:js", function () {
	return gulp.src(paths.compiledJsList, { base: "." })
	  .pipe(concat(paths.concatJsDest))
	  .pipe(gulp.dest("."));
});

gulp.task("concat:ts", function () {
	return gulp.src(paths.tsList, { base: "." })
		.pipe(concat(paths.concatTsDest))
		.pipe(gulp.dest("."));
})

gulp.task("concat", ["concat:ts", "clean"]);

gulp.task("min:js", function () {
	return gulp.src([paths.concatJsDest])
	  .pipe(uglify())
	  .pipe(rename({ suffix: ".min" }))
	  .pipe(gulp.dest(paths.jsDest));
});

gulp.task("min", ["min:js", "concat"])

gulp.task("all", ["min"]);